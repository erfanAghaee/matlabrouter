function cells = getCells()
cells = [
"AND2X1" , "0.700000" , "1.200000" , 
"AND2X2" , "0.700000" , "1.200000" , 
"AND2X4" , "0.900000" , "1.200000" , 
"AND2XL" , "0.700000" , "1.200000" , 
"AND3XL" , "0.900000" , "1.200000" , 
"AND4X1" , "1.100000" , "1.200000" , 
"AND4X2" , "1.100000" , "1.200000" , 
"AND4X4" , "2.000000" , "1.200000" , 
"AND4XL" , "1.100000" , "1.200000" , 
"AOI21X1" , "0.700000" , "1.200000" , 
"AOI21X4" , "1.800000" , "1.200000" , 
"AOI21XL" , "0.700000" , "1.200000" , 
"AOI221X1" , "1.300000" , "1.200000" , 
"AOI221X2" , "2.100000" , "1.200000" , 
"AOI221X4" , "2.000000" , "1.200000" , 
"AOI222X1" , "1.400000" , "1.200000" , 
"AOI222X2" , "2.500000" , "1.200000" , 
"AOI222X4" , "2.100000" , "1.200000" , 
"AOI22X1" , "0.900000" , "1.200000" , 
"AOI22X2" , "1.600000" , "1.200000" , 
"AOI22X4" , "2.500000" , "1.200000" , 
"AOI22XL" , "0.900000" , "1.200000" , 
"BUFX12" , "1.800000" , "1.200000" , 
"BUFX16" , "2.300000" , "1.200000" , 
"BUFX2" , "0.700000" , "1.200000" , 
"BUFX20" , "2.800000" , "1.200000" , 
"BUFX3" , "0.700000" , "1.200000" , 
"BUFX4" , "0.900000" , "1.200000" , 
"BUFX6" , "1.300000" , "1.200000" , 
"BUFX8" , "1.600000" , "1.200000" , 
"DLY1X1" , "1.100000" , "1.200000" , 
"DLY2X1" , "1.100000" , "1.200000" , 
"DLY3X1" , "1.300000" , "1.200000" , 
"DLY4X1" , "1.300000" , "1.200000" , 
"INVX1" , "0.400000" , "1.200000" , 
"INVX12" , "2.300000" , "1.200000" , 
"INVX2" , "0.600000" , "1.200000" , 
"INVX3" , "0.700000" , "1.200000" , 
"INVX4" , "0.700000" , "1.200000" , 
"INVX8" , "1.100000" , "1.200000" , 
"INVXL" , "0.400000" , "1.200000" , 
"MX2XL" , "1.400000" , "1.200000" , 
"MXI2XL" , "1.300000" , "1.200000" , 
"NAND2BX1" , "0.700000" , "1.200000" , 
"NAND2BX2" , "1.100000" , "1.200000" , 
"NAND2BXL" , "0.700000" , "1.200000" , 
"NAND2X1" , "0.600000" , "1.200000" , 
"NAND2X2" , "0.900000" , "1.200000" , 
"NAND2X4" , "1.300000" , "1.200000" , 
"NAND3BX2" , "1.400000" , "1.200000" , 
"NAND3X1" , "0.700000" , "1.200000" , 
"NAND3X2" , "1.300000" , "1.200000" , 
"NAND3X4" , "1.800000" , "1.200000" , 
"NAND4BX1" , "1.300000" , "1.200000" , 
"NAND4BX2" , "1.800000" , "1.200000" , 
"NAND4BX4" , "3.200000" , "1.200000" , 
"NAND4X1" , "0.900000" , "1.200000" , 
"NAND4X2" , "1.600000" , "1.200000" , 
"NAND4X4" , "3.000000" , "1.200000" , 
"NOR2BX1" , "0.700000" , "1.200000" , 
"NOR2BX4" , "1.400000" , "1.200000" , 
"NOR2X1" , "0.600000" , "1.200000" , 
"NOR2X2" , "0.900000" , "1.200000" , 
"NOR2X4" , "1.300000" , "1.200000" , 
"NOR2XL" , "0.600000" , "1.200000" , 
"NOR3BX1" , "1.100000" , "1.200000" , 
"NOR3BX4" , "2.000000" , "1.200000" , 
"NOR3X1" , "0.700000" , "1.200000" , 
"NOR3X2" , "1.300000" , "1.200000" , 
"NOR3X4" , "1.800000" , "1.200000" , 
"NOR4BBX2" , "2.000000" , "1.200000" , 
"NOR4X1" , "1.100000" , "1.200000" , 
"NOR4X2" , "1.600000" , "1.200000" , 
"NOR4X4" , "3.000000" , "1.200000" , 
"NOR4XL" , "1.100000" , "1.200000" , 
"OAI221X1" , "1.400000" , "1.200000" , 
"OAI22X1" , "1.100000" , "1.200000" , 
"OAI22X2" , "1.600000" , "1.200000" , 
"OAI22XL" , "1.100000" , "1.200000" , 
"OAI2BB1X1" , "0.900000" , "1.200000" , 
"OR2X1" , "0.700000" , "1.200000" , 
"OR2X2" , "0.700000" , "1.200000" , 
"OR2X4" , "1.100000" , "1.200000" , 
"OR3X1" , "1.100000" , "1.200000" , 
"OR3X4" , "1.600000" , "1.200000" , 
"OR4X1" , "1.100000" , "1.200000" , 
"OR4X2" , "1.100000" , "1.200000" , 
"OR4X4" , "2.000000" , "1.200000" , 
"SDFFHQX2" , "4.200000" , "1.200000" , 
"SDFFHQX4" , "4.500000" , "1.200000" , 
"SEDFFHQX1" , "5.400000" , "1.200000" , 
"SEDFFHQX2" , "5.900000" , "1.200000" , 
"XOR2X1" , "1.400000" , "1.200000" 
];
end