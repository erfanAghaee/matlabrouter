
%% Init Types
% close all
clear
clc
fig =figure;


dieType   = 0;
cellType  = 1;
pinType   = 2;
GCellType = 3;
netGuideType    = 4;
choiceGuideType = 5;

global cells
global instances
global nets
global netGuides
global hplot



%% Draw Die
%sample 1
% rect.xl = 83600;
% rect.yl = 71820;
% rect.xh = 104400;
% rect.yh = 91200;

%test 1 
% rect.xl = 0;
% rect.yl = 0;
% rect.xh = 296800;
% rect.yh = 292000;

% rect.xl = 173000;
% rect.yl = 176100;
% rect.xh = 202000;
% rect.yh = 202100;

rect.xl = 233000;
rect.yl = 335300;
rect.xh = 600000;
rect.yh = 900000;

% 234001 336301 239999 347699 5 0 0 0 0; 
% 294001 524401 299999 530099 5 294200 524590 294200 529910; 
% 792001 877801 797999 3921599 5 0 0 0 0; 


dieArea = rect;
numLayers = 9;

axis([dieArea.xl dieArea.xh dieArea.yl dieArea.yh 1 numLayers])
grid on

% areaDie = rectangle('rect2D',dieArea, 0, dieType);
% areaDie2 = rectangle('rect2D',dieArea, 1, dieType);
% areaDie2 = rectangle('rect2D',dieArea, 2, dieType);


%% Draw Cells
cells     = getCells();
instances = getInstances();


areaRectTotal = 0;
for i = 1 : size(instances(:,1),1)
    inst = instances(i,:);

    rectInst = getInstCell(inst);

    areaRectTmp = rectangle('rect2D',rectInst, 0, cellType);
    areaRectTotal = areaRectTmp + areaRectTotal;
end % end for 

%% Draw Net Guides
% netGuides = getNetGuides();
% for i = 1 : size(netGuides(:,1),1)
%     netGuide = netGuides(i,:);
%     
%     guideRect   = getGuideRect(netGuide);
%     areaRectTmp = rectangle('rect2D',guideRect, netGuide(5), netGuideType+netGuide(5));
%     
% 
% %     areaRectTotal = areaRectTmp + areaRectTotal;
% end % end for 



%% Choice Print
% for i = 1 : size(netGuides(:,1),1)
%     netGuide = netGuides(i,:);
%     if(netGuide(5) == 0)
%         continue
%     end  
%     choiceRect  = getChoiceRect(netGuide);
%     areaRectTmp = rectangle('rect2D',choiceRect, netGuide(5), choiceGuideType+netGuide(5));
%     
% 
% %     areaRectTotal = areaRectTmp + areaRectTotal;
% end % end for 



%% Draw Pin in Cell

%% Draw Nets
% nets = getNets();

%% Draw GCElls
% dieAreaNew = dieArea/3;
% rectangle('rect2D',dieArea./3, 0, GCellType);

%% Do some maths
% Density = areaDie/areaRectTotal;



%% Cursor manager


%% local Functions
function rectInst = getInstCell(inst)
    cellName = inst(1,2);
    cellInfo = getCell(cellName);
    width  = str2double( cellInfo(1,2) ) * 2000;
    height = str2double( cellInfo(1,3) ) * 2000;
    rectInst.xl = str2double( inst(1,3) ) ;
    rectInst.yl = str2double( inst(1,4) ) ;
    rectInst.xh = str2double( inst(1,3) ) + width;
    rectInst.yh = str2double( inst(1,4) ) + height;
end % end method 

function cellInfo = getCell(cellName)
    global cells
    for i = 1 : size(cells(:,1),1)
        if(strcmp(cells(i,1),cellName))
            cellInfo = cells(i,:);
        end % end if 
    end % end for 
end % end method 

function  guideRect = getGuideRect(netGuide)
guideRect.xl = netGuide(1);
guideRect.yl = netGuide(2);
guideRect.xh = netGuide(3);
guideRect.yh = netGuide(4);
end 

function choiceRect    = getChoiceRect(netGuide)
choiceRect.xl = netGuide(6);
choiceRect.yl = netGuide(7);
choiceRect.xh = netGuide(8);
choiceRect.yh = netGuide(9);
end 



