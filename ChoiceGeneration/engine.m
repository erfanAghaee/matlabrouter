%% input 
clear all;
close all;
clc

data_input   = zeros(10,1);
data_input(1,1) = 1;
data_input(10,1) = 1;
data_sort  = 0;


for i = 1 : size(data_input,1)
    if( data_input(i,1) == 0)
        data_sort = [data_sort, i];
    end 
end 

data_sort = data_sort(2:end);

wire.start = 0;
wire.stop  = 0;
wires = wire;
i = 1;
for j = 1 : size(data_sort,2)-1 
    wire.start = i;
    if(abs(data_sort(1,j)-data_sort(1,j+1)) ~= 1)
        if( i ~= j)
            wire.stop = j;
            wires = [wires, wire];
            i = j+1;
        end 
    elseif(j+1 == size(data_sort,2))
        wire.stop = j+1;
        wires = [wires, wire];
        i = j+1;
    end
    
    
    
end 

wires = wires(2:end);

for i = 1 : size(wires,2)
    wires(i).start = data_sort(1,wires(i).start);
    wires(i).stop  = data_sort(1,wires(i).stop);
end 


