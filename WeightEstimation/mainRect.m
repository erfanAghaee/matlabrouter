
%% Init Types
close all
clear
clc
fig =figure;

% hold on 
alpha('color')
grid on;

%% Test Bench survey
test_bench = 'ispd19_sample1';


dieType   = 0;
cellType  = 1;
pinType   = 2;
GCellType = 3;
netGuideType    = 4;
choiceGuideType = 5;



global cells
global instances
global nets
global netGuides
global hplot
global cellInsts



%% Draw Die
%sample 1
% rect.xl = 83600;
% rect.yl = 71820;
% rect.xh = 104400;
% rect.yh = 91200;

%test 1 
% rect.xl = 0;
% rect.yl = 0;
% rect.xh = 296800;
% rect.yh = 292000;

% rect.xl = 173000;
% rect.yl = 176100;
% rect.xh = 202000;
% rect.yh = 202100;

rect.xl = 233000;
rect.yl = 335300;
rect.xh = 600000;
rect.yh = 900000;

% 234001 336301 239999 347699 5 0 0 0 0; 
% 294001 524401 299999 530099 5 294200 524590 294200 529910; 
% 792001 877801 797999 3921599 5 0 0 0 0; 


dieArea = rect;
numLayers = 9;

axis([dieArea.xl dieArea.xh dieArea.yl dieArea.yh 1 numLayers])
grid on

% areaDie = rectangle('rect2D',dieArea, 0, dieType);
% areaDie2 = rectangle('rect2D',dieArea, 1, dieType);
% areaDie2 = rectangle('rect2D',dieArea, 2, dieType);


%% Draw Cells
cells     = getCells(test_bench);
instances = getInstances(test_bench);
initCells('ispd19_sample1');


areaRectTotal = 0;
for i = 1 : size(instances(:,1),1)
    inst = instances(i,:);

    [rectInst,cellName] = getInstCell(inst);

    areaRectTmp = rectangle('rect2D',rectInst, 1, cellType);
    areaRectTotal = areaRectTmp + areaRectTotal;
    drawCell(cellName,rectInst,inst(1,3),inst(1,4),inst(1,5));
    
end % end for 

%% Draw Net Guides
% netGuides = getNetGuides();
% for i = 1 : size(netGuides(:,1),1)
%     netGuide = netGuides(i,:);
%     
%     guideRect   = getGuideRect(netGuide);
%     areaRectTmp = rectangle('rect2D',guideRect, netGuide(5), netGuideType+netGuide(5));
%     
% 
% %     areaRectTotal = areaRectTmp + areaRectTotal;
% end % end for 



%% Choice Print
% for i = 1 : size(netGuides(:,1),1)
%     netGuide = netGuides(i,:);
%     if(netGuide(5) == 0)
%         continue
%     end  
%     choiceRect  = getChoiceRect(netGuide);
%     areaRectTmp = rectangle('rect2D',choiceRect, netGuide(5), choiceGuideType+netGuide(5));
%     
% 
% %     areaRectTotal = areaRectTmp + areaRectTotal;
% end % end for 



%% Draw Pin in Cell

%% Draw Nets
% nets = getNets();

%% Draw GCElls
% dieAreaNew = dieArea/3;
% rectangle('rect2D',dieArea./3, 0, GCellType);

%% Do some maths
% Density = areaDie/areaRectTotal;



%% Cursor manager


%% local Functions
function [rectInst,cellName] = getInstCell(inst)
    cellName = inst(1,2);
    cellInfo = getCell(cellName);
    width  = str2double( cellInfo(1,2) );
    height = str2double( cellInfo(1,3) );
    rectInst.xl = str2double( inst(1,3) ) ;
    rectInst.yl = str2double( inst(1,4) ) ;
    rectInst.xh = str2double( inst(1,3) ) + width;
    rectInst.yh = str2double( inst(1,4) ) + height;
end % end method 

function cellInfo = getCell(cellName)
    global cells
    for i = 1 : size(cells(:,1),1)
        if(strcmp(cells(i,1),cellName))
            cellInfo = cells(i,:);
        end % end if 
    end % end for 
end % end method 

function  guideRect = getGuideRect(netGuide)
guideRect.xl = netGuide(1);
guideRect.yl = netGuide(2);
guideRect.xh = netGuide(3);
guideRect.yh = netGuide(4);
end 

function choiceRect    = getChoiceRect(netGuide)
choiceRect.xl = netGuide(6);
choiceRect.yl = netGuide(7);
choiceRect.xh = netGuide(8);
choiceRect.yh = netGuide(9);
end 


function initCells(testBench)
    global cellInsts
    cellInsts = Cell.empty
    pins  = Pin.empty
    obss  = OBS.empty
    rects = Rect.empty
    
    cellPins = getCellPins(testBench);
    
    numCell = 0;
    numPin  = 0;
    numOBS  = 0;
    bPinRect = 0;
    bOBSRect = 0;
    for i = 1 : size(cellPins,1) 
          if(cellPins(i,1) == "macro") 
              numPin = 0;
              pins = Pin.empty;
              numOBS = 0;
              obss = OBS.empty;
              numCell = numCell + 1;
              cellName = cellPins(i,2);
              cellInsts(numCell) = Cell(cellName);
          elseif(cellPins(i,1) == "pin")             
              numPin = numPin + 1;
              pinName = cellPins(i,2);
              pins(numPin) = Pin(pinName);
              cellInsts(numCell).setPin(pins(numPin));
              bPinRect = 1;
              bOBSRect = 0;
          elseif(cellPins(i,1) == "OBS")          
              numOBS = numOBS + 1;
              obsName = cellPins(i,2);
              obss(numOBS) = OBS(obsName);
              cellInsts(numCell).setOBS(obss(numOBS));
              bPinRect = 0;
              bOBSRect = 1;
          else
              xl = str2double(cellPins(i,1));
              yl = str2double(cellPins(i,2));
              xh = str2double(cellPins(i,3));
              yh = str2double(cellPins(i,4));
              layer      = str2double(cellPins(i,5));
              rect = Rect(xl,yl,xh,yh,layer);
              if(bOBSRect == 0 && bPinRect==1)
                cellInsts(numCell).setPinRect(pins(numPin),rect);
              elseif(bOBSRect == 1 && bPinRect==0)
                cellInsts(numCell).setOBSRect(obss(numOBS),rect);
              else
                disp('invalid rectangle for pin and obs!!!')
              end

          end 

    end 

end

function drawCell(cellName,rectInst,x,y,orientation)
    global cellInsts
    for i = 1 : size(cellInsts,2)
        if(cellInsts(i).name == cellName)
            cell = cellInsts(i);
            cellRects = cell.getRects();
            for j = 1 : size(cellRects,2)
                newRect = shiftRectV2(cellRects(j),rectInst,x,y,orientation)
%                newRect = shiftRect(cellRects(j),x,y)
               drawRect('rect2D',newRect,1)
            end 
        end 
    end 
    
end 

function rect = shiftRect(rectTmp,x,y)
    rect.xl = rectTmp.xl + str2double(x);
    rect.yl = rectTmp.yl + str2double(y);
    rect.xh = rectTmp.xh + str2double(x);
    rect.yh = rectTmp.yh + str2double(y);
    rect.layer = rectTmp.layer;
end 


function rect = shiftRectV2(rectTmp,rectInst,xLocation,yLocation,sw)
w = abs(rectInst.xl-rectInst.xh);
h = abs(rectInst.yl-rectInst.yh);
xLocation = str2double(xLocation);
yLocation = str2double(yLocation);
%% Lower Bound
x = rectTmp.xl;
y = rectTmp.yl;

[matTransform,matTranslate] = TransformMatrix(sw);
mx = (x*matTransform(1,1)) + (y*matTransform(1,2));
my = (x*matTransform(2,1)) + (y*matTransform(2,2));

tx = (w*matTranslate(1,1)) + (h*matTranslate(1,2));
ty = (w*matTranslate(2,1)) + (h*matTranslate(2,2));


rect.xl = mx+tx;
rect.yl = my+ty;
%% Upper Bound
x = rectTmp.xh;
y = rectTmp.yh;

[matTransform,matTranslate] = TransformMatrix(sw);
mx = (x*matTransform(1,1)) + (y*matTransform(1,2));
my = (x*matTransform(2,1)) + (y*matTransform(2,2));

tx = (w*matTranslate(1,1)) + (h*matTranslate(1,2));
ty = (w*matTranslate(2,1)) + (h*matTranslate(2,2));
rect.xh = mx+tx;
rect.yh = my+ty;


xl = min(rect.xl,rect.xh);
yl = min(rect.yl,rect.yh);
xh = max(rect.xl,rect.xh);
yh = max(rect.yl,rect.yh);

rect.xl = xl + xLocation;
rect.yl = yl + yLocation;
rect.xh = xh + xLocation;
rect.yh = yh + yLocation;;
rect.layer = rectTmp.layer;
end 









