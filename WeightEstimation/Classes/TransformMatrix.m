function [matTransform,matTranslate] = TransformMatrix(sw)

coef = 1;
switch sw
    case 'N'%% North Orientation (R0)
        coef = 1;
    case 'S'%% South orientation (R180)
        coef = 2;
    case 'W'%% West orientation (R90)
        coef = 3;
    case 'E'%% East orientation  (R270)
        coef = 4
    case 'FN' %% Flipped-North orientation (MY)
        coef = 5   
    case 'FS' %% Flipped-South orientation (MX)
        coef = 6 
    case 'FW' %% Flipped-West orientation (MX90)
        coef = 7 
    case 'FE' %% Flipped-East orientation (MY90)
        coef = 8
    otherwise
        disp('other invalid orientation')
end
    
mat = zeros(2,2);



	%% NORTH (R0)
matTransformTotal = [
     1,  0;             %% | cos(0)  sin(0)|
	 0,  1;             %% |-sin(0)  cos(0)|

	%% SOUTH (R180)
	-1,  0;             %% | cos(-180)  sin(-180)|
	 0, -1;            %% |-sin(-180)  cos(-180)|

	%% WEST (R90)
	 0, -1;             %% | cos(-90)  sin(-90)|
	 1,  0;            %% |-sin(-90)  cos(-90)|

	%% EAST (R270)
	 0,  1;             %% | cos(-270)  sin(-270)|
	-1,  0;            %% |-sin(-270)  cos(-270)|

	%% FLIPPED NORTH (MY)
	-1,  0;             %% |-1  0|
	 0,  1;            %% | 0  1|

	%% FLIPPED SOUTH (MX)
	 1,  0;             %% |1   0|
	 0, -1;             %% |0  -1|

	%% FLIPPED WEST (MX90)
	 0,  1;             %% |1   0| | cos(90)  sin(90)|
	 1,  0;            %% |0  -1| |-sin(90)  cos(90)|

	%% FLIPPED EAST (MY90)
	 0, -1;             %% |-1  0| | cos(90)  sin(90)|
	-1,  0];             %% | 0  1| |-sin(90)  cos(90)|

matTransform(1,:) = matTransformTotal((coef*2)-1,:);
matTransform(2,:) = matTransformTotal(coef*2,:);



matTranslateTotal =[ 
     %% NORTH (R0)
      0,  0;               %% 0
	  0,  0;               %% 0

	%% SOUTH (R180)
	  1,  0;               %% w
	  0,  1;               %% h

	%% WEST (R90)
	  0,  1;               %% h
	  0,  0;               %% 0

	%% EAST (R270)
	  0,  0;               %% 0
	  1,  0;               %% w

	%% FLIPPED NORTH (MY)
	  1,  0;               %% w
	  0,  0;               %% 0

	%% FLIPPED SOUTH (MX)
	  0,  0;               %% 0
	  0,  1;               %% h

	%% FLIPPED WEST (MX90)
	  0,  0;               %% 0
	  0,  0;               %% 0

	%% FLIPPED EAST (MY90)
	  0,  1;               %% h
	  1,  0;               %% w
];

matTranslate(1,:) = matTranslateTotal((coef*2)-1,:);
matTranslate(2,:) = matTranslateTotal(coef*2,:);


end 

