classdef Cell < handle
    %CELL Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        name
        pins = Pin.empty;
        numPin = 0;
        obss = OBS.empty;
        numOBS = 0;
        
    end
    
    methods
        function obj = Cell(name)            
            obj.name = name; 
        end
        
        function obj = setPin(obj,Pin_Obj)
            for i = 1 : size(Pin_Obj,2)
                obj.numPin = obj.numPin + 1;
                obj.pins(obj.numPin) = Pin_Obj(i);
            end 
        end 
        
        function obj = setOBS(obj,OBS_Obj)
            for i = 1 : size(OBS_Obj,2)
                obj.numOBS = obj.numOBS + 1;
                obj.obss(obj.numOBS) = OBS_Obj(i);
            end 
        end
        
        function obj = setPinRect(obj,Pin_Obj,Rect_Obj)
            for i = 1 : obj.numPin
                if(obj.pins(i).name == Pin_Obj.name)
                    obj.pins(i).setRect(Rect_Obj);
                end 
            end 
        end 
        
        function obj = setOBSRect(obj,OBS_Obj,Rect_Obj)
            for i = 1 : obj.numOBS
                if(obj.obss(i).name == OBS_Obj.name)
                    obj.obss(i).setRect(Rect_Obj);
                end 
            end 
        end 
        
        function rects = getRects(obj)
            rects = Rect.empty;
            for i = 1 : size(obj.pins,2)
                rectTmp = obj.pins(i).rects;
                rects = [rects, rectTmp];
            end
            for i = 1 : size(obj.obss,2)
                rectTmp = obj.obss(i).rects;
                rects = [rects, rectTmp];
            end 
        end 
        
        
        
%         function obj = Cell(name,Pin_Obj)            
%             obj.name = name; 
%             for i = 1 : size(Pin_Obj,2)
%                 obj.pins(i) = Pin_Obj(i);
%             end 
%         end
        
    end
end

