rect.xl = 1; rect.xh = 2; rect.yl = 3; rect.yh = 4; layer = 1

poly = Polygon(rect,layer)

rect.xl = 5; rect.xh = 6; rect.yl = 7; rect.yh = 8; layer = 2
poly(end + 1) = Polygon(rect,layer)
rect.xl = 9; rect.xh = 10; rect.yl = 11; rect.yh = 12; layer = 3
poly(end + 1) = Polygon(rect,layer)

pin = Pin("A")

pin.addPolygon(poly)