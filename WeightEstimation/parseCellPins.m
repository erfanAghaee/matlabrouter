function test = parseCellPins(cellName)
test = 0;

% global cellInsts

initCells();
drawCell(cellName);


%% make view transparent
hold on 
alpha('color')
grid;




% vert = [0 1 0; 1 0 0; 0 0 1];
% patch('Vertices', vert, 'Faces', 1:3, 'FaceColor', 'cyan');
% hold on
% alpha('color')

% text(0.3,0.3,0.4, 'OBS');


function drawCell(cellName)
    global cellInsts
    for i = 1 : size(cellInsts,2)
        if(cellInsts(i).name == cellName)
            cell = cellInsts(i);
            cellRects = cell.getRects();
            for j = 1 : size(cellRects,2)
                drawRect('rect2D',cellRects(j),1)
            end 
        end 
    end 
    
end 



function initCells()
    global cellInsts
    cellInsts = Cell.empty
    pins  = Pin.empty
    obss  = OBS.empty
    rects = Rect.empty
    
    cellPins = getCellPins('ispd19_sample1');
    
    numCell = 0;
    numPin  = 0;
    numOBS  = 0;
    bPinRect = 0;
    bOBSRect = 0;
    for i = 1 : size(cellPins,1) 
          if(cellPins(i,1) == "macro") 
              numPin = 0;
              pins = Pin.empty;
              numOBS = 0;
              obss = OBS.empty;
              numCell = numCell + 1;
              cellName = cellPins(i,2);
              cellInsts(numCell) = Cell(cellName);
          elseif(cellPins(i,1) == "pin")             
              numPin = numPin + 1;
              pinName = cellPins(i,2);
              pins(numPin) = Pin(pinName);
              cellInsts(numCell).setPin(pins(numPin));
              bPinRect = 1;
              bOBSRect = 0;
          elseif(cellPins(i,1) == "OBS")          
              numOBS = numOBS + 1;
              obsName = cellPins(i,2);
              obss(numOBS) = OBS(obsName);
              cellInsts(numCell).setOBS(obss(numOBS));
              bPinRect = 0;
              bOBSRect = 1;
          else
              xl = str2double(cellPins(i,1));
              yl = str2double(cellPins(i,2));
              xh = str2double(cellPins(i,3));
              yh = str2double(cellPins(i,4));
              layer      = str2double(cellPins(i,5));
              rect = Rect(xl,yl,xh,yh,layer);
              if(bOBSRect == 0 && bPinRect==1)
                cellInsts(numCell).setPinRect(pins(numPin),rect);
              elseif(bOBSRect == 1 && bPinRect==0)
                cellInsts(numCell).setOBSRect(obss(numOBS),rect);
              else
                disp('invalid rectangle for pin and obs!!!')
              end

          end 

    end 

end

end

