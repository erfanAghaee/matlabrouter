function areaSC = rectangle(sw,rect,z,type)
    switch sw
        case 'rect2D'
            disp('draw Rectangle2D')
            rect2D(rect.xl, rect.yl, rect.xh, rect.yh,z,type)
        case 0
            disp('zero')
        case 1
            disp('positive one')
        otherwise
            disp('other value')
    end % end switch
    
    areaSC = area(rect);
end % end method 

function rect2D(xl, yl, xh, yh,z, type)

% p1 = gpuArray([xl yl z]);
% p2 = gpuArray([xh yl z]);
% p3 = gpuArray([xh yh z]);
% p4 = gpuArray([xl yh z]);

p1 = ([xl yl z]);
p2 = ([xh yl z]);
p3 = ([xh yh z]);
p4 = ([xl yh z]);


% The points must be in the correct sequence.
% The coordinates must consider x, y and z-axes.
x = [p1(1) p2(1) p3(1) p4(1)];
y = [p1(2) p2(2) p3(2) p4(2)];
z = [p1(3) p2(3) p3(3) p4(3)];
fill3(x, y, z, type);
grid
hold on
end % end function

function areaSC = area(rect)
    areaSC = (rect.xh - rect.xl )*(rect.yh - rect.yl );
end 
