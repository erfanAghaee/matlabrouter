time = 0:0.5:10;
values = rand(1,length(time));
datatip_index = [3 6]; % an 'event' occurs on the 3rd and 6th datapoint 
datatip_text = {'event1', 'event2'};

figure;
plot(time,values)
hold on
text(time(datatip_index), values(datatip_index), datatip_text)