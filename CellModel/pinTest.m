pinName = "A";
pin = Pin(pinName);
rect    = Rect(1,2,3,4,5);
pin = pin.setRect(rect);
rect(1) = Rect(2,5,3,4,6);
pin = pin.setRect(rect(1));
rect(2) = Rect(3,6,3,4,7);
pin = pin.setRect(rect(2));
rect(3) = Rect(4,7,3,4,8);
pin = pin.setRect(rect(3));

pin = Pin(pinName);


i = 2;
rect2    = Rect(1*i,2,3,4,5);
rect2(1) = Rect(2*i,5,3,4,6);
rect2(2) = Rect(3*i,6,3,4,7);
rect2(3) = Rect(4*i,7,3,4,8);
pinName = "B";
pin(2) = Pin(pinName,rect2);

cellName = "AND2X1";
cell = Cell(cellName, pin);
