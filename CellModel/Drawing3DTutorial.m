%%Step #1 - Drawing the triangle
vert = [0 1 0; 1 0 0; 0 0 1];
patch('Vertices', vert, 'Faces', 1:3, 'FaceColor', 'cyan');
view(3)
set(gca, 'xdir', 'reverse')
set(gca, 'ydir', 'reverse')
grid;
alpha('color')
%% meshgrid
[X,Y] = meshgrid(0:(1/6):1);
Z = 1 - X - Y;
Z(Z < -0.01) = NaN;
hold on;
plot3(X, Y, Z, 'b.', 'MarkerSize', 32);
%% Adding the text box
text(0.3,0.3,0.4, 'NE');
line([1/2 1/3], [1/2 1/3], [0 1/3], 'color', 'black');
xlabel('R');
ylabel('P');
zlabel('S');