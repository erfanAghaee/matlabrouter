classdef OBS < handle
    %PIN Summary of this class goes here
    %   Detailed explanation goes here    
    properties
        name 
        rects = Rect.empty;
        numRect = 0;
    end
    
    methods
        function obj = OBS(name)
            %PIN Construct an instance of this class
            %   Detailed explanation goes here
            obj.name    = name;
        end
        
        function obj = setRect(obj,Rect_Obj)
            for i = 1 : size(Rect_Obj,2)
                obj.numRect = obj.numRect + 1;
                obj.rects(obj.numRect) = Rect_Obj(i);
            end 
        end 
        
    end
end



