classdef Rect < handle
    %POLYGON Summary of this class goes here
    %   Detailed explanation goes here
    
    properties 
        xl 
        yl
        xh
        yh        
        layer
    end
    
    methods 
        function obj = Rect(xl,yl,xh,yh,layer)
            %POLYGON Construct an instance of this class
            %   Detailed explanation goes here
            obj.xl = xl;
            obj.yl = yl;
            obj.xh = xh;
            obj.yh = yh;
            obj.layer   = layer;
            
        end
        
        function obj = setRect(obj,rect,layer)
            %POLYGON Construct an instance of this class
            %   Detailed explanation goes here
            obj.xl = rect.xl;
            obj.yl = rect.yl;
            obj.xh = rect.xh;
            obj.yh = rect.yh;
            obj.layer   = layer;
        end         
    end
end

